package domain.com.springproject.example.controller;

import domain.com.springproject.example.model.CounterRequest;
import domain.com.springproject.example.service.CounterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/counter")
public class CounterController {

    @Autowired
    private CounterService counterService;

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    //increase simple by connecting to website ++
    public int increment(){
        return counterService.increment();
    }

    @PostMapping("/count")
    @ResponseStatus(HttpStatus.OK)
    //incrase by specific number in body of request
    public int incrementByBody(@RequestBody CounterRequest counterRequest){
        return counterService.incrementBy(counterRequest.getCounter());
    }

    @GetMapping("/{number}")
    @ResponseStatus(HttpStatus.OK)
    //increase by param in url /counter/x
    public int incrementByParam(@PathVariable("number")int number){
        return counterService.incrementBy(number);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public int getCounter(){
        return counterService.getCounter();
    }


}
