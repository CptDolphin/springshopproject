package domain.com.springproject.example.service;


import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class CounterService {

    public int counter = 0;

    public int increment() {
        return this.counter++;
    }

    public int incrementBy(int howMuch){
        this.counter+=howMuch;
        return counter;
    }

    public int getCounter() {
        return counter;
    }
}
