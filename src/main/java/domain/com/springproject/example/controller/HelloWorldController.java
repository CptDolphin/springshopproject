package domain.com.springproject.example.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/hello")
public class HelloWorldController {

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public String returnWord(@RequestBody String word){
        return word;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public String returnHello(){
        return "Hello World";
    }
}
